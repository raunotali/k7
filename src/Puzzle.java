import java.util.LinkedHashSet;

public class Puzzle {

   private Node[] nodes;
   private int solutions = 0;

   /** Solve the word puzzle.
    * @param args three words (addend1 addend2 sum)
    */
   public static void main (String[] args) {
         Puzzle puzzle = new Puzzle();
         String[] words = new String[]{"ABCDEFGHIJAB", "ABCDEFGHIJA", "ACEHJBDFGIAC"};
         long startTime = System.currentTimeMillis();
         puzzle.runProgram(words);
         long endTime = System.currentTimeMillis();
         String second = (endTime - startTime) / 1000 == 1 ? " second" : " seconds";
         System.out.println("That took " + (endTime - startTime) /
                 1000 + second + " and " + (endTime - startTime) % 1000 + " milliseconds");
   }

   public static class Node {

      char letter;
      long value;

      @Override
      public String toString() {
         return letter + "=" + value;
      }

      @Override
      public boolean equals(Object obj) {
         if (obj == this) return true;
         if (!(obj instanceof Node)) return false;
         Node node = (Node) obj;
         return this.letter == node.letter;
      }
   }

   private void validate(String word1, String word2, String word3) {

      for (Node node : nodes) {
         if (node.letter == word1.charAt(0) || node.letter == word2.charAt(0) || node.letter == word3.charAt(0)) {
            if (node.value == 0) {
               return;
            }
         }
      }

      long value1 = calculateWordValue(word1);
      long value2 = calculateWordValue(word2);
      long value3 = calculateWordValue(word3);

      if ((value1 + value2) == value3) {
         solutions++;
         if (solutions == 1) {
            System.out.println(word1 + " + " + word2 + " = " + word3);
            System.out.println(value1 + " + " + value2 + " = " + value3);
            for (Node node : nodes) {
               if (Character.isLetter(node.letter)) {
                  System.out.print(node + " ");
               }
            }
            System.out.println();
         }
      }
   }

   private long calculateWordValue(String word) {

      long value = 0;
      int multiplier = 1;
      for (int i = word.length() - 1; i >= 0; i--) {
         char letter = word.toCharArray()[i];
         for (Node node : nodes) {
            if (node.letter == letter) {
               value += node.value * multiplier;
               multiplier *= 10;
               break;
            }
         }
      }
      return value;
   }

   private void solvePuzzle(String word1, String word2, String word3, char[] letters) {

      nodes = new Node[letters.length];

      int nodeCount = 0;
      for (char letter : letters) {
         Node node = new Node();
         node.letter = letter;
         node.value = 0;
         nodes[nodeCount] = node;
         nodeCount++;
      }

      check(word1, word2, word3, new boolean[10], 0);

      if (solutions == 0) {
         System.out.println("No solutions found!");
      } else {
         System.out.println("Number of solutions: " + solutions);
      }
   }

   private boolean check(String word1, String word2, String word3, boolean[] visited, int currentNode) {
      if (nodes.length == currentNode) {
         validate(word1, word2, word3);
         return false;
      }
      for (int i = 0; i < 10; i++) {
         if (!visited[i]) {
            nodes[currentNode].value = i;
            visited[i] = true;
            if (check(word1, word2, word3, visited, currentNode + 1)) {
               return true;
            }
            visited[i] = false;
         }
      }
      return false;
   }

   private void runProgram(String[] words) {
      LinkedHashSet<String> lettersTemp = new LinkedHashSet<>();
      for (String word : words) {
         for (char letter : word.toCharArray()) {
            lettersTemp.add(String.valueOf(letter));
         }
      }

      if (lettersTemp.size() > 10) {
         throw new RuntimeException("Too many unique letters! Impossible to solve word puzzle.");
      }

      char[] letters = new char[lettersTemp.size()];
      int i = 0;
      for (String stringLetter : lettersTemp) {
         char letter = stringLetter.charAt(0);
         letters[i] = letter;
         i++;
      }

      solvePuzzle(words[0], words[1], words[2], letters);

   }
}